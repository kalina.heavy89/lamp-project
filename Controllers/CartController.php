<?php

namespace LampProject\Controllers;

use LampProject\Models\Product;
use LampProject\Routers\Router;

class CartController extends BaseController
{
    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Merge all repeated records in $cart.
     *
     * @param string $cartCookies
     * @return array
     */
    public static function cartUnique(array $cartDecode = []): array
    {
        //defining all exists $productId
        $ids = array_column($cartDecode, "productId");
        $idsUnique = array_unique($ids);
        //Create array $cartUnique with unique keys
        foreach ($idsUnique as $uniq) {
            foreach ($cartDecode as $key => $value) {
                if ($uniq == $value["productId"]) {
                    $cartUnique[$uniq][] = $value["quantity"];
                }
            }
        }
        //Assemble array with keys of unique "productId" and "quantity"
        //that is sum of selected products quantity with unique ids
        if (isset($cartUnique)) {
            foreach ($cartUnique as $key => $value) {
                $cart["productId"][] = $key;
                $cart["quantity"][] = array_sum($value);
            }
        } else {
            $cart["productId"] = null;
            $cart["quantity"] = null;
        }
        return $cart;
    }

    public static function cartTable(int $productId, int $quantity): array
    {
        $product = Product::getById($productId);
        //$product = json_decode(json_encode($productJson), true);
        unset($product->properties);
        $product = (array) $product;
        $product["price"] = $product["price"];
        $product["selected_quantity"] = $quantity;
        $product["totalProductPrice"] = $product["price"] * $product["selected_quantity"];
        return $product;
    }

    private function deleteProductFromCart(int $productId)
    {
        $productCookies = json_decode($_COOKIE['cart'], true);
        $i = 0;
        foreach ($productCookies as $productCookie) {
            if ($productCookie["productId"] == $productId) {
                break;
            }
            $i++;
        }
        unset($productCookies[$i]);
        $productCookies = self::cartUnique($productCookies);
        return $productCookies;
    }

    public function index(array $cart = null)
    {
        $cartSize = 0;

        if (isset($_COOKIE['cart'])) {
            if (empty($cart)) {
                $cartDecode = json_decode($_COOKIE['cart'], true);
                $cart = self::cartUnique($cartDecode);
            }
            if (! empty($cart["productId"])) {
                $cartSize = count($cart["productId"]);
            }
        }

        $totalPrice = 0;
        if ($cartSize) {
            for ($i = 0; $i < count($cart["productId"]); $i++) {
                $cartTable[$i] = self::cartTable($cart["productId"][$i], $cart["quantity"][$i]);
                $totalPrice += (float) $cartTable[$i]["totalProductPrice"];
            }
        } else {
            $cartTable = [];
        }

        $this->view->render('cart', ["products" => $cartTable, "totalPrice" => $totalPrice, "cartSize" => $cartSize]);
    }

    public function add(array $request = null)
    {
        $cartDecode = json_decode($_COOKIE['cart'], true);
        $cart = self::cartUnique($cartDecode);
        $cartSize = count($cart["productId"]);
        echo json_encode(
            [
                'statusCode' => 200,
                'cartSize' => $cartSize
            ]
        );
    }

    public function delete(array $request = null)
    {
        $productId = $request["delete_product"];
        $cart = $this->deleteProductFromCart($productId);
        $this->index($cart);
    }

}
