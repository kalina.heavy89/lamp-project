<?php

namespace LampProject\Controllers;

use Exception as GlobalException;
use Mpdf\Mpdf;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
class OrderController extends BaseController
{
    private const ORIGIN_ADDRESS = [
        'regionalCenter' => 'Poltava',
        'city' => 'Kremenchuk',
        'street' => 'Nebesnoi Sotni',
        'streetNumber' => '30',
        'postalCode' => '39600'
    ];

    private const SITE_ROUTE_ESTIMATION = 'https://api.tomtom.com/routing/1/calculateRoute/';
    private const ROUTE_SETTINGS = 'json?
        instructionsType=text&language=en-US
        &vehicleHeading=90&sectionType=traffic
        &report=effectiveSettings&routeType=eco
        &traffic=true&avoid=unpavedRoads
        &travelMode=car&vehicleMaxSpeed=120
        &vehicleCommercial=false&vehicleEngineType=combustion';
    private const STRUCTURED_GEOCODE_MAIN = "https://api.tomtom.com/search/2/structuredGeocode.JSON";
    private const SPECIFIC_DISTANCE_PRICE = 10;
    private const SHOP_ADDRESS = 'myshop@test.com';
    private const SHOP_NAME = 'My Shop';
    private const BUYER_ADDRESS = 'buyer@test.com';
    private const BUYER_NAME = 'Buyer';

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function index(array $request)
    {
        $cartSize = count($request["id"]);
        $totalPrice = $request["total-price"];
        unset($request["total-price"]);
        $tableFields = ["id", "name", "image", "quantity", "totalProductPrice"];
        for ($i = 0; $i < $cartSize; $i++) {
            $j = 0;
            foreach ($request as $value) {
                $orderData[$i][$tableFields[$j]] = $value[$i];
                $j++;
            }
        }
        $this->view->render('order', ["orderData" => $orderData, "totalPrice" => $totalPrice, "cartSize" => $cartSize]);
    }

    private function transformationStringToUrl (string $string)
    {
        return preg_replace('/\s+/', '', $string);
    }

    private function requestToUrl (string $url)
    {
        try {
            $response = [];
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', $url);
            $response["statusCode"] = $res->getStatusCode(); // "200"
            $response["format"] = $res->getHeader('content-type')[0]; // 'application/json; charset=utf8'
            $response["data"] = json_decode($res->getBody(), true);
        } catch (\Exception $e) {
            echo 'Guzzle: something goes wrong!';
            die;
        }
        return $response;
    }

    private function getCoordinates($regionalCenter, $city, $streetName, $homeNumber, $postalCode)
    {
        $response = [];
        $coordinates = [];
        $requestRouting = (
            self::STRUCTURED_GEOCODE_MAIN .
            '?key=' . $_ENV['API_TOMTOM_DELIVERY_KEY'] .
            '&countryCode=' . 'UA' .
            '&streetNumber=' . $homeNumber .
            '&streetName=' . $streetName .
            '&municipality=' . $city .
            '&municipalitySubdivision=' . $regionalCenter .
            '&countrySecondarySubdivision=' . 'Ukraine'.
            '&postalCode=' . $postalCode
        );
        $requestRoutingString = $this->transformationStringToUrl($requestRouting);
        $response = $this->requestToUrl($requestRoutingString);

        $coordinates[] = 'error';
        if (! isset($response['data'])) { //Structured Geocode API response is empty
            $coordinates[] = 1;
            return $coordinates;
        }
        if (empty($response['data']['results'])) { //Delivery address wasn't fount
            $coordinates[] = 2;
            return $coordinates;
        }
        if (count($response['data']['results']) >= 1) {
            $coordinates[0] = 'ok';
            $coordinates[1] = $response['data']['results'][0]['position']['lat'];
            $coordinates[2] = $response['data']['results'][0]['position']['lon'];
            return $coordinates;
        }
    }

    private function getDeliveryDistance($originLatitude, $originLongitude, $deliveryLatitude, $deliveryLongitude)
    {
        $response = [];
        $deliveryData = [];
        $requestRouting = (
            self::SITE_ROUTE_ESTIMATION .
            $originLatitude . ',' .
            $originLongitude . ":" .
            $deliveryLatitude . ',' .
            $deliveryLongitude . "/" .
            self::ROUTE_SETTINGS . "&key=" .
            $_ENV['API_TOMTOM_DELIVERY_KEY']
        );

        $requestRoutingString = $this->transformationStringToUrl($requestRouting);
        $response = $this->requestToUrl($requestRoutingString);

        $deliveryData[] = 'error';
        $deliveryData[] = 3; //Calculate Route API response is empty

        $deliveryDistance = $response["data"]["routes"][0]["summary"]["lengthInMeters"] ?? null;

        if ($deliveryDistance) {
            $deliveryData[0] = 'ok';
            $deliveryData[1] = $deliveryDistance;
        }

        return $deliveryData;
    }

    public function delivery(array $request) {
        if (
            empty($request['regional-center']) ||
            empty($request['city']) ||
            empty($request['street-name']) ||
            empty($request['home-number']) ||
            empty($request['postal-code'])
        ) {
            $delivery[] = 'error';
            $delivery[] = 4;
            echo json_encode($delivery);
            return;
        }

        $delivery = [];

        $originCoordinates = $this->getCoordinates(
            self::ORIGIN_ADDRESS['regionalCenter'],
            self::ORIGIN_ADDRESS['city'],
            self::ORIGIN_ADDRESS['street'],
            self::ORIGIN_ADDRESS['streetNumber'],
            self::ORIGIN_ADDRESS['postalCode']
        );

        //$originCoordinates[0] = 'error';
        //$originCoordinates[1] = 1;

        if ($originCoordinates[0] != 'ok') {
            echo json_encode($originCoordinates);
            return;
        }

        $deliveryCoordinates = $this->getCoordinates(
            $request['regional-center'],
            $request['city'],
            $request['street-name'],
            $request['home-number'],
            $request['postal-code']
        );

        //$deliveryCoordinates[0] = 'error';
        //$deliveryCoordinates[1] = 2;

        if ($deliveryCoordinates[0] != 'ok') {
            echo json_encode($deliveryCoordinates);
            return;
        }

        $delivery = $this->getDeliveryDistance($originCoordinates[1], $originCoordinates[2], $deliveryCoordinates[1], $deliveryCoordinates[2]);

        //$delivery[0] = 'error';
        //$delivery[1] = 3;

        if ($delivery[0] == 'ok') {
            $delivery[2] = round(($delivery[1] / 1000 * self::SPECIFIC_DISTANCE_PRICE), 0);
        }

        echo json_encode($delivery);
    }

    private function reportEmail($pdfFilePath, $pdfFileName, $addressFrom, $nameFrom, $addressTo, $nameTo, $textMessage)
    {
        // create a new object
        $mail = new PHPMailer();
        // configure an SMTP
        $mail->isSMTP();
        $mail->Host = $_ENV['MAIL_HOST'];
        $mail->SMTPAuth = true;
        $mail->Username = $_ENV['MAIL_USERNAME'];
        $mail->Password = $_ENV['MAIL_PASSWORD'];
        $mail->SMTPSecure = $_ENV['MAIL_ENCRYPTION'];
        $mail->Port = $_ENV['MAIL_PORT'];

        $mail->setFrom($addressFrom, $nameFrom);
        $mail->addAddress($addressTo, $nameTo);
        $mail->Subject = 'Thanks for choosing Our Shop!';
        // Set HTML
        $mail->isHTML(TRUE);
        $mail->Body = $textMessage;
        $mail->AltBody = $textMessage;
        // add attachment
        $mail->addAttachment($pdfFilePath . $pdfFileName);
        // send the message
        if(!$mail->send()){
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            //echo 'Message has been sent';
        }
    }

    public function report(array $request)
    {
        $htmlString = $this->view->createOrderPdf($request);
        $directoryIcons = ROOT_PATH . "/public/icons/shopping_cart.png";
        $directoryFiles = ROOT_PATH . "/public/files/";
        $pdf = new Mpdf();
        $pdf->WriteHtml($htmlString);
        $pdf->Image($directoryIcons, 100, 10, 30, 30, 'jpg', '', true, true, false, true, true);
        $currentDate = date("dmYGis", strtotime('+3 hours'));
        $fileName = 'order' . $currentDate . '.pdf';
        $pdf->Output($directoryFiles . $fileName, 'F');
        $textMessage = 'Hi there, we are happy to confirm your order. Please check the document in the attachment.';

        $this->reportEmail(
            $directoryFiles,
            $fileName,
            self::SHOP_ADDRESS,
            self::SHOP_NAME,
            self::BUYER_ADDRESS,
            self::BUYER_NAME,
            $textMessage
        );

        unlink($directoryFiles . $fileName);

        $this->view->render('orderSuccess');
    }
}