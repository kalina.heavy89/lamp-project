<?php

namespace LampProject\Controllers;

use \LampProject\Views\View;
use \LampProject\Routers\Router;

class BaseController
{
    public $view;
    protected $exception = [];

    /**
     * Constructor that crete instance of class View for connecting templates.
     *
     * @return void
     */
    public function __construct()
    {
        $this->view = new View();
    }
}