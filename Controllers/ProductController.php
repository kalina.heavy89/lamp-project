<?php

namespace LampProject\Controllers;

use LampProject\Models\Product;
use LampProject\Models\Category;
use LampProject\Controllers\CartController;
use LampProject\Routers\Router;

class ProductController extends BaseController
{
    public const PRODUCTS_PER_PAGE = 6;

    /**
     * Constructor to inheritance parent constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get all records from table products to view products.
     *
     * @return void
     */
    public function index(array $request = null)
    {
        $productCategory = [];
        if (isset($request["product_category"])) {
            foreach($request["product_category"] as $category) {
                $clearedCategory = (int) trim($category, " \n\r\t\v\0");
                if ($clearedCategory != 0) {
                    $productCategory[] = $clearedCategory;
                }
            }
        }
        if (empty($productCategory)) {
            $request["product_category"] = "*";
            $productCategory[] = $request["product_category"];
        }

        $currentPage = isset($request["num_page"]) ? $request["num_page"] : 1;

        $products = Product::getProductsWithPagination(self::PRODUCTS_PER_PAGE, $currentPage, $productCategory);
        $pages = $products['pages'];
        unset($products['pages']);
        if ($currentPage > $pages) {
            $currentPage = $pages;
        }
        if ($currentPage < 1) {
            $currentPage = 1;
        }

        foreach ($products as &$product) {
            $product->category = Category::getById($product->categoryId)->name;
        }

        $cartSize = 0;
        if (isset($_COOKIE['cart'])) {
            $productCookies = json_decode($_COOKIE['cart'], true);
            $cart = CartController::cartUnique($productCookies);
            if (! empty($cart["productId"])) {
                $cartSize = count($cart["productId"]);
            }
        }

        $this->view->render('products',
            [
                "products" => $products,
                "pages" => $pages,
                "currentPage" => $currentPage,
                "productCategory" => $productCategory,
                "cartSize" => $cartSize
            ]
        );
    }
}
