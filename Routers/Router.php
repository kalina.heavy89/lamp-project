<?php
    namespace LampProject\Routers;

    class Router
    {
        private $routes = [];
        public static $currentAction;

        /**
         * Constructor. Initialization routes.
         *
         * @return void
         */
        public function __construct()
        {
            $this->init();
        }

        /**
         * Execute redirect.
         *
         * @param string $url
         * @return void
         */
        public static function redirect(string $url)
        {
            header("Location: " . $url);
            die();
        }

        /**
         * Set route to array routes[].
         *
         * @param string $url
         * @param string $controller
         * @param string $action
         * @return bool
         */
        public function setRoute(string $url, string $controller, string $action): bool
        {
            if (empty($this->routes[$url])) {
                $this->routes[$url] = ["controller" => $controller, "action" => $action];
                return true;
            }
            return false;
        }

        /**
         * Init all routes and write it to array routes[].
         *
         * @return void
         */
        public function init()
        {
            $this->setRoute("/", "Product", "index");
            $this->setRoute("cart", "Cart", "index");
            $this->setRoute("cart/add", "Cart", "add");
            $this->setRoute("cart/delete", "Cart", "delete");
            $this->setRoute("order", "Order", "index");
            $this->setRoute("order/delivery", "Order", "delivery");
            $this->setRoute("order/report", "Order", "report");
            //$this->setRoute("icons", "Icons", )
        }

        /**
         * Get route from array routes[].
         *
         * @param string $url
         * @return array
         */
        public function getRoute(string $url): array
        {
            return $this->routes[$url] ?? [];
        }

        /**
         * Process route from $url and $request.
         *
         * @param string $url
         * @return array
         */
        public function process(string $url, array $request)
        {
            $route = $this->getRoute($url);//Get existing route from array route[]
            if (empty($route)) { //if route doesn't exist - go to 404 error page
                die('111');
                $controller = new \LampProject\Controllers\ErrorController();
                $controller->error404($request);
                return;
            }
            self::$currentAction = $route['action'];
            $controllerName = "\\LampProject\\Controllers\\" . $route['controller'] . "Controller";
            $controller = new $controllerName();
            $controller->{$route['action']}($request); // ProductController->index($request)
        }
    }