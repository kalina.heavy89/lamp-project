function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
}
return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}

function parseCookie(nameCookie) {
    let data = JSON.parse(readCookie(nameCookie)); //get cookie cart
    if (data === null) {
        data = JSON.parse("[]");
    }
    return data;
}

function addProductToCartCookie(productId, quantity) {
    let prod = parseCookie("cart");
    let item = {
        "productId": productId,
        "quantity": quantity
    }
    prod.push(item);
    createCookie('cart', JSON.stringify(prod), 1); //set cookie cart
}

function updateCartCookie(arrayId, arrayQuantity) {
    let prod = [];
    let item;
    for (let i = 0; i < arrayId.length; i++) {
        item = {
            "productId": arrayId[i],
            "quantity": arrayQuantity[i]
        }
        prod.push(item);
    }
    createCookie('cart', JSON.stringify(prod), 1);
}

function findProductKey(parseCookies, productId) {
    if (Array.isArray(parseCookies) && parseCookies.length) {
        for(let key in parseCookies) {
            if (parseCookies[key]["productId"] == productId) {
                return key;
            }
        }
        return null;
    } else {
        return null;
    }
}

function deleteProductFromCartCookie(productId) {
    let prod = parseCookie("cart");
    let productKey = findProductKey(prod, productId);
    if (productKey != null) {
        prod.splice(productKey, 1);
        createCookie('cart', JSON.stringify(prod), 1);
    }
}

function changeProductQuantityFromCartCookie(productId, changeQuantity) {
    let prod = parseCookie("cart");
    let productKey = findProductKey(prod, productId);
    if (productKey != null) {
        prod[productKey]["quantity"] = changeQuantity;
        createCookie('cart', JSON.stringify(prod), 1);
    }
}
