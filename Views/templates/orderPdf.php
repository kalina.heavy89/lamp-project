<main>

        <h4 class='date'>Date: <?php echo date("d M Y"); ?> year</h4>

        <table>
            <tr>
                <th>Number</th>
                <th>Product Name</th>
                <th>Quantity</th>
                <th>Product prices</th>
            </tr>
            <?php $tableLength = count($orderData['id']);
            for ($i = 0; $i < $tableLength; $i++): ?>
                <tr>
                    <td><?php echo ($i + 1); ?></td>
                    <td><?php echo $orderData['name'][$i]; ?></td>
                    <td><?php echo $orderData['quantity'][$i]; ?></td>
                    <td><?php echo $orderData['product-price'][$i]; ?></td>
                </tr>
            <?php endfor; ?>
        </table>
        <br>
        <ul>
            <?php if ($orderData["delivery-distance"]): ?>
                <li>Product Price: <?php echo $orderData["total-product-price"]; ?></li>
                <li>Delivery Distance: <?php echo $orderData["delivery-distance"]; ?></li>
                <li>Delivery Price: <?php echo $orderData["delivery-price"]; ?></li>
            <?php endif; ?>
            <li>Total Price: <?php echo $orderData["total-price"]; ?></li>
        </ul>
        <br>

</main>