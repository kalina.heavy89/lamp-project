<main>

    <div class="card">
        <form method="post" action="/order">
            <h2 class="card-title">Cart</h2>
            <div class="card-body not-empty-cart">
                <table class="table table-bordered" style="width: 60%;">
                    <thead>
                    <tr>
                        <th>Product Name</th>
                        <th style="width: 50px">Image</th>
                        <th>Price Per Unit</th>
                        <th style="width: 100px">Quantity</th>
                        <th>Total Product Price</th>
                        <th>Delete Product</th>
                    </tr>
                    </thead>
                    <tbody class="cart">
                        <?php $countProduct = 1;
                        $productIds =[];
                        foreach ($products as $product): ?>
                            <tr class="item">
                                <td><?php echo $product["name"]; ?>
                                    <input name="name[]" value="<?php echo $product["name"]; ?>" hidden>
                                </td>
                                <td>
                                    <?php if (!empty($product["image"])): ?>
                                        <img class="img-fluid product-img cart-img" src="<?php echo $product["image"]; ?>" >
                                        <input name="image[]" value="<?php echo $product["image"]; ?>" hidden>
                                    <?php else: ?>
                                        <svg class="bd-placeholder-img card-img-top" width="100%" height="225"
                                            xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice"
                                            focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>
                                            Placeholder</title>
                                            <rect width="100%" height="100%" fill="#55595c"/>
                                            <text x="50%" y="50%" fill="#eceeef" dy=".3em">
                                                No image
                                            </text>
                                            <input name="image[]" value="<?php echo null; ?>" hidden>
                                        </svg>
                                    <?php endif; ?>
                                </td>
                                <td id="product-price-<?php echo $product["id"]; ?>"><?php echo number_format($product["price"], 2, '.', ''); ?></td>
                                <td>
                                    <input type="number" class="select-quantity" id="quantity-<?php echo $product["id"]; ?>" name="quantity[]" value="<?php echo $product["selected_quantity"]; ?>" data-min="1" data-max="<?php echo $product["quantity"]; ?>">
                                </td>
                                <td>
                                    <span id="total-product-price-table-<?php echo $product["id"]; ?>"><?php echo number_format($product["totalProductPrice"], 2, '.', ''); ?></span>
                                    <input id="total-product-price-<?php echo $product["id"]; ?>" name="total-product-price[]" value="<?php echo $product["totalProductPrice"]; ?>" hidden>
                                </td>
                                <td>
                                    <button class="btn btn-danger delete-from-cart" id="delete-product<?php echo $countProduct; ?>" name="delete_product" value="<?php echo $product["id"]; ?>">
                                        Delete
                                    </button>
                                </td>
                            </tr>
                            <input name="id[]" value="<?php echo $product["id"]; ?>" hidden>

                        <?php $numberProduct[] = $countProduct;
                        $productIds[] = $product["id"];
                        $quantity[] = $product["selected_quantity"];
                        $countProduct++;
                        endforeach; ?>
                    </tbody>
                </table>
                <br>
                <div class="container order-table">
                    <div class="row">
                        <div class="right-align" id="total-price-page">
                            Total Price: <?php echo number_format($totalPrice, 2, '.', ''); ?>
                        </div>
                        <input id="total-price" name="total-price" value="<?php echo $totalPrice; ?>" hidden>
                    </div>
                    <br>
                    <div class="row">
                        <p class="right-align">
                            <button type="submit" class="button-order" value="order">To order</button>
                        </p>
                    </div>
                </div>
            </div>
        </form>
        <div class="empty-cart">
            <h3 class="card-title">Your cart is empty</h3>
        </div>
    </div>

</main>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
</script>
<script src="../scripts/cookies.js">
</script>
<script>

    $(document).ready(function(){
        $("#products").hide();
        $('#resume-shopping').show();
        $("#shopping-cart").hide();
        $(".navbar-category").hide();

        let cartCookies = parseCookie('cart');
        if (Array.isArray(cartCookies) && cartCookies.length) {
            $(".empty-cart").hide();
        } else {
            $(".not-empty-cart").hide();
            $(".empty-cart").show();
            $(".order").hide();
        }

        function findKeys(values, array) {
            let findedKeys = [];
            for (let value of values) {
                for (key in array) {
                    if (value == array[key]) {
                        findedKeys.push(Number(key));
                        break;
                    }
                }
            }
            return findedKeys;
        }

        function getNumProducts() {
            let cartCookies = parseCookie('cart');
            let productIds = [];
            if (Array.isArray(cartCookies) && cartCookies.length) {
                for(key in cartCookies) {
                    productIds.push(cartCookies[key]["productId"]);
                }
            } else {
                return null;
            }
            let productIdsTable;
            <?php if (! empty($productIds)): ?>
                productIdsTable = <?php echo json_encode($productIds); ?>;
            <?php else: ?>
                return null;
            <?php endif; ?>
            if (productIds) {
                return findKeys(productIds, productIdsTable);
            } else {
                return null;
            }
        }

        function computeTotalPrice() {
            let numProducts = getNumProducts();
            if (Array.isArray(numProducts) && numProducts.length) {
                let totalPrice = 0;
                let ids = <?php echo json_encode($productIds); ?>
                //let indexes = findKeys(values, array)
                for (let numProduct of numProducts) {
                    //index = numProduct + 1;
                    totalPrice += parseFloat($("#total-product-price-" + (ids[numProduct])).val());
                }
                totalPrice = totalPrice.toFixed(2);
                $("#total-price-page").html("Total Price: " + totalPrice);
                $("#total-price").val(totalPrice);
            } else {
                $(".not-empty-cart").hide();
                $(".empty-cart").show();
                $(".order").hide();
            }
        }

        //refresh cookies
        $(".cart").ready(function() {
            let productId;
            let quantity;
            <?php if (! empty($productIds)): ?>
                productId = <?php echo json_encode($productIds); ?>;
                quantity = <?php echo json_encode($quantity); ?>;
            <?php else: ?>
                productId = 0;
                quantity = 0;
            <?php endif; ?>

            updateCartCookie(productId, quantity);
        });

        //Delete selected product
        $(".delete-from-cart").on("click", function(e) {
            e.preventDefault();
            let idToDelete = $(this).val();
            $(this).parents(".item").remove();
            deleteProductFromCartCookie(idToDelete);
            //compute total price
            computeTotalPrice();
        });

        //Change quantity from selected product
        $(".select-quantity").on("input", function () {
            let maxQuantity = $(this).data("max");
            if ($(this).val() > maxQuantity) {
                $(this).val(maxQuantity);
            }
            if ($(this).val() < 1) {
                $(this).val(1);
            }

            let splitStr = $(this).attr("id").split("-");
            let productId = splitStr[1];
            let changeQuantity = $(this).val();
            changeProductQuantityFromCartCookie(productId, changeQuantity);
            let productPrice = $("#product-price-" + productId).html();
            let totalProductPrice = parseFloat(productPrice) * parseFloat(changeQuantity);
            $("#total-product-price-" + productId).val(totalProductPrice.toFixed(2));
            $("#total-product-price-table-" + productId).html(totalProductPrice.toFixed(2));
            computeTotalPrice();
        });

    });

</script>