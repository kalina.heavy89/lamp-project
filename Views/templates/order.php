<?php
    $deliveryPrice = 0;
?>

<main>

    <div class="card">
        <h2 class="card-title">Order</h2>
        <div class="card-body not-empty-cart">

            <form method="post" action="/order/report">

                <table class="table table-bordered order-table">
                    <thead>
                    <tr>
                        <th>Number</th>
                        <th>Product Name</th>
                        <th style="width: 50px">Image</th>
                        <th>Quantity</th>
                        <th>Product Prices</th>
                    </tr>
                    </thead>
                    <tbody class="cart">
                        <?php $countProduct = 1;
                        $productIds =[];
                        foreach ($orderData as $product): ?>
                            <tr class="item">
                                <td><?php echo $countProduct; ?></td>
                                <td><?php echo $product["name"]; ?>
                                    <input name="name[]" class="product-name" value="<?php echo $product["name"]; ?>" hidden>
                                </td>
                                <td>
                                    <?php if (!empty($product["image"])): ?>
                                        <img class="img-fluid product-img cart-img" src="<?php echo $product["image"]; ?>" >
                                        <input name="image[]" value="<?php echo $product["image"]; ?>" hidden>
                                    <?php else: ?>
                                        <svg class="bd-placeholder-img card-img-top" width="100%" height="225"
                                            xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice"
                                            focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>
                                            Placeholder</title>
                                            <rect width="100%" height="100%" fill="#55595c"/>
                                            <text x="50%" y="50%" fill="#eceeef" dy=".3em">
                                                No image
                                            </text>
                                            <input name="image[]" value="<?php echo null; ?>" hidden>
                                        </svg>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php echo $product["quantity"]; ?>
                                    <input class="select-quantity" id="quantity-<?php echo $product["id"]; ?>" name="quantity[]" value="<?php echo $product["quantity"]; ?>" data-min="1" data-max="<?php echo $product["quantity"]; ?>" hidden>
                                </td>
                                <td>
                                    <span id="product-price-table-<?php echo $product["id"]; ?>"><?php echo number_format((float) $product["totalProductPrice"], 2, '.', ''); ?></span>
                                    <input id="product-price-<?php echo $product["id"]; ?>" name="product-price[]" value="<?php echo number_format($product["totalProductPrice"], 2, '.', ''); ?>" hidden>
                                </td>
                            </tr>
                            <input name="id[]" value="<?php echo $product["id"]; ?>" hidden>

                        <?php $numberProduct[] = $countProduct;
                        $productIds[] = $product["id"];
                        $quantity[] = $product["quantity"];
                        $countProduct++;
                        endforeach; ?>
                    </tbody>
                </table>

                <div class="container order-table">
                    <div class="row">
                        <div class="right-align" id="total-product-price-page">
                            Product Price: <?php echo number_format($totalPrice, 2, '.', ''); ?>
                        </div>
                        <input id="total-product-price" name="total-product-price" value="<?php echo $totalPrice; ?>" hidden>
                    </div>
                    <div class="row">
                        <div class="center-align">
                            Delivery Method:
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="left-align">
                            <input type="radio" class="delivery" id="self-pickup" name="delivery" value="self-pickup" checked="checked">
                            <label for="self-pickup">Self-pickup from shop</label>
                            <br>
                            <input type="radio" class="delivery" id="courier" name="delivery" value="courier">
                            <label for="courier">Courier delivery</label>
                        </div>
                    </div>
                    <br>
                    <form method="post" action="/order/delivery" autocomplete="on">
                        <div class="delivery-calculate">
                            <div class="row">
                                <div class="col">
                                    <label for="regional-center">Regional Center: </label>
                                </div>
                                <div class="col">
                                    <input type="text" class="address-delivery" id="regional-center" name="regional-center">
                                </div>
                                <div class="col-6">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col">
                                    <label for="city">City: </label>
                                </div>
                                <div class="col">
                                    <input type="text" class="address-delivery" id="city" name="city">
                                </div>
                                <div class="col-6">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col">
                                    <label for="street-name">Street Name: </label>
                                </div>
                                <div class="col">
                                    <input type="text" class="address-delivery" id="street-name" name="street-name">
                                </div>
                                <div class="col-6">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col">
                                    <label for="home-number">Home Number: </label>
                                </div>
                                <div class="col">
                                    <input type="text" class="address-delivery" id="home-number" name="home-number">
                                </div>
                                <div class="col-6">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col">
                                    <label for="postal-code">Postal Code: </label>
                                </div>
                                <div class="col">
                                    <input type="text" class="address-delivery" id="postal-code" name="postal-code">
                                </div>
                                <div class="col-6">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <strong id="delivery-error" class="text-danger"></strong>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col">
                                    <button type="submit" class="button-select-category float-beg mb-1" id="calculate">Calculate</button>
                                </div>
                                <div class="col">
                                </div>
                            </div>
                        </div>
                    </form>
                    <br>
                    <div class="row">
                        <div class="col">
                        <p class="float-end mb-1 delivery-calculate" id="delivery-distance-page">
                                Delivery Distance: 0.0 kilometers
                            </p>
                            <input id="delivery-distance" name="delivery-distance" value="<?php echo $deliveryPrice; ?>" hidden>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col">
                            <p class="float-end mb-1" id="delivery-price-page">
                                Delivery Price: <?php echo number_format($deliveryPrice, 2, '.', ''); ?>
                            </p>
                            <input id="delivery-price" name="delivery-price" value="<?php echo $deliveryPrice; ?>" hidden>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="right-align" id="total-price-page">
                            Total Price: <?php echo number_format(($totalPrice + $deliveryPrice), 2, '.', ''); ?>
                        </div>
                        <input id="total-price" name="total-price" value="<?php echo ($totalPrice + $deliveryPrice); ?>" hidden>
                    </div>
                    <br>
                <!--<form method="post" action="/order/report">-->
                    <div class="row">
                        <div class="col">
                        </div>
                        <div class="col">
                            <button type="submit" class="button-order float-end mb-1">Order</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</main>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
</script>
<script src="../scripts/cookies.js">
</script>
<script>

    $(document).ready(function(){
        $("#products").hide();
        $('#resume-shopping').hide();
        $("#shopping-cart").show();
        $(".navbar-category").hide();
        $(".delivery-calculate").hide();
        $('#delivery-error').hide();

        $(".delivery").on("change", function() {
            if ($("#self-pickup").prop("checked")) {
                $(".delivery-calculate").hide();
            } else {
                $(".delivery-calculate").show();
            }
        });

        function checkFillFields(arrayFields) {
            for (let value of arrayFields) {
                if (! value) {
                    throw Error('All fields must be filled');
                    return false;
                }
            }
            return true;
        }

        function checkDeliveryResponse(arrayResponse) {
            if (arrayResponse[0] != 'ok') {
                if (arrayResponse[1] == 1) {
                    throw Error('Structured Geocode API response is empty');
                }
                if (arrayResponse[1] == 2) {
                    throw Error('Delivery address wasn\'t fount');
                }
                if (arrayResponse[1] == 3) {
                    throw Error('Calculate Route API response is empty');
                }
                if (arrayResponse[1] == 4) {
                    throw Error('All delivery fields must be filled');
                }
            }
        }

        $("#calculate").on("click", function(e) {
            e.preventDefault();

            $('#delivery-error').hide();

            let deliveryFields = [];
            let regionalCenter = $("#regional-center").val();
            deliveryFields.push(regionalCenter);
            let city = $("#city").val();
            deliveryFields.push(city);
            let streetName = $("#street-name").val();
            deliveryFields.push(streetName);
            let homeNumber = $("#home-number").val();
            deliveryFields.push(homeNumber);
            let postalCode = $("#postal-code").val();
            deliveryFields.push(postalCode);

            /*try {
                checkFillFields(deliveryFields);
            } catch (error) {
                $('#delivery-error').html(error.message);
                $('#delivery-error').show();
                return;
            }*/

            $.ajax({
                url: "/order/delivery",
                type: "POST",
                data: {
                    "regional-center": regionalCenter,
                    "city": city,
                    "street-name": streetName,
                    "home-number": homeNumber,
                    "postal-code": postalCode
                }
            }).done(function(resp) {
                let response = JSON.parse(resp);
                console.log(resp);
                try {
                    checkDeliveryResponse(response);
                } catch (error) {
                    $('#delivery-error').html(error.message);
                    $('#delivery-error').show();
                    return;
                }
                let distance = Math.round(response[1]/100)/10;
                //console.log(distance);
                $('#delivery-distance-page').html('Delivery Distance: ' + distance.toFixed(1) + ' kilometers');
                $('#delivery-distance').val(distance);
                let deliveryPrice = response[2];
                $('#delivery-price-page').html('Delivery Price: ' + deliveryPrice.toFixed(2));
                $('#delivery-price').val(deliveryPrice);
                let totalPrice = <?php echo $totalPrice; ?>;
                totalPrice = Number(totalPrice + deliveryPrice);
                //console.log(totalPrice);
                $("#total-price-page").html('Total Price: ' + totalPrice.toFixed(2));
                $('#total-price').val(totalPrice);
            }).fail(function(xhr) {
                switch (xhr.status) {
                    case 404:
                        alert('404 status code! not found!');
                        break;
                    case 400:
                        alert('400 status code! user error');
                        break;
                    case 500:
                        alert('500 status code! server error');
                        break;
                }
            });
        });

    });

</script>