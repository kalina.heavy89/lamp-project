<?php
    $categoryNumbers = ["*", "1", "2", "3", "4"];
    $categoryNames = ["All categories", "Food", "Clothes", "Shoes", "Dishes"];

    function parseProductCategory($productCategory = [])
    {
      $stringProductCategory = "";
      if ($productCategory == "*") {
        return "&product_category[]=*";
      }
      foreach ($productCategory as $category) {
        $stringProductCategory .= "&product_category[]=" . $category;
      }
      return $stringProductCategory;
    }

    function checkSelectedCategory($currentCategory, $selectedCategory, $currentCategoryCounter, $selectedCategoryCounter)
    {
        if (isset($selectedCategory[$selectedCategoryCounter-1])) {
            if ($currentCategory[$currentCategoryCounter] == $selectedCategory[$selectedCategoryCounter - 1]) {
              return true;
            }
            return false;
        } else {
            return false;
        }
    }
?>

<main>

  <div class="container">
    <p class="float-beg mb-1">
      <form action="/">
        <?php $j = 1;
          for ($i = 0; $i < count($categoryNumbers); $i++): ?>
          <input type="<?php
            if ($i == 0):
              ?>radio<?php
            else:
              ?>checkbox<?php
            endif; ?>" class="product_category" name="product_category[]" value="<?php
            echo $categoryNumbers[$i]; ?>" <?php
            if (checkSelectedCategory($categoryNumbers, $productCategory, $i, $j)):
              $j++; ?>checked<?php
            endif;?>><?php
            echo $categoryNames[$i];
          ?><br/>
        <?php endfor; ?>
        <input name="num_page" class="product_category" id="product_category_page" value="<?php echo $currentPage; ?>" hidden>
        <input type="submit" id="submit_product_category" value="Select Category">
      </form>
    </p>
  </div>

  <div class="album py-5 bg-light">
    <div class="container">
      <form method="post" action="/cart/add">
        <div class="row">
          <?php $countProduct = 1;
            foreach ($products as $product): ?>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <?php if (!empty($product->getImage())): ?>
                    <img class="img-fluid product-img" src="<?php echo $product->getImage(); ?>" >
                <?php else: ?>
                  <svg class="bd-placeholder-img card-img-top" width="100%" height="225"
                      xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice"
                      focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>
                      Placeholder</title>
                      <rect width="100%" height="100%" fill="#55595c"/>
                      <text x="50%" y="50%" fill="#eceeef" dy=".3em">
                            <?php echo $product->getName(); ?>
                      </text>
                  </svg>
                <?php endif; ?>
                <div class="card-body">
                  <table class="table">
                    <thead>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Product Name</td>
                        <td><?php echo $product->getName(); ?></td>
                      <tr>
                        <td>Available Quantity</td>
                        <td><?php echo $product->getQuantity(); ?></td>
                      </tr>
                      <tr>
                        <td>Category Name</td>
                        <td><?php echo $product->category; ?></td>
                      </tr>
                      <tr>
                        <td>Price</td>
                        <td><?php echo number_format($product->getPrice(), 2, '.'); ?> UAH</td>
                      </tr>
                    </tbody>
                  </table>
                  <div class="d-flex justify-content-between align-items-center">
                    <button class="btn btn-success add-to-cart" type="submit" id="submit_add_to_cart" name="add_to_cart[]" value="<?php echo $countProduct; ?>">
                      Add to Cart
                    </button>
                    <input id="productId<?php echo $countProduct; ?>" name="productId" value="<?php echo $product->getId(); ?>" hidden>
                    <input name="num_product" class="num_product" value="<?php echo $countProduct; ?>" hidden>
                    <input type="number" id="quantity<?php echo $countProduct; ?>" name="quantity" value="1" min="1" max="<?php echo $product->getQuantity(); ?>">Quantity
                  </div>
                </div>
                <div id="success<?php echo $countProduct; ?>" class="alert alert-success success" role="alert">
                  <i class="icon fas fa-check">
                      <div style="text-align: center;">
                        Product was added
                      </div>
                  </i>
                </div>
              </div>
            </div>
          <?php
            $countProduct++;
            endforeach;
          ?>
        </div>
        <input type="submit" class="btn btn-success" value="Order">
      </form>

        <div class="container" id="pagination">
          <?php if ($pages > 1): ?>
            <br>
            <p class="float-beg mb-1" class="sub-page">
              <?php if ($currentPage > 1): ?>
                <a href="/?num_page=<?php echo ($currentPage - 1) . parseProductCategory($productCategory); ?>">Prev</a>
              <?php endif;?>
              <?php for ($i = 0; $i < $pages; $i++): ?>
                <a href="/?num_page=<?php echo ($i + 1) . parseProductCategory($productCategory); ?>" id="sub-page-num<?php echo ($i + 1); ?>"><?php echo $i + 1; ?></a>
              <?php endfor; ?>
              <?php if ($currentPage < $pages): ?>
                <a href="/?num_page=<?php echo ($currentPage + 1) . parseProductCategory($productCategory); ?>">Next</a>
              <?php endif;?>
            </p>
          <?php endif;?>
        </div>

    </div>

    <div id="result"></div>

  </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
  </script>
  <script>
  $(document).ready(function(){
    $(".success").hide();

    $('input[class="product_category"]').on('change', function(){
    if ($(this).attr('type') == 'radio' ) {
        if ( $(this).prop('checked') ) {
            $('input[class="product_category"][type="checkbox"]').prop('checked', false);
        }
    }
    else {
        if ( $(this).prop('checked') ) {
            $('input[class="product_category"][type="radio"]').prop('checked', false);
        }
    }
    });

    $("#pagination").ready(function() {
      if ($('#sub-page').ready()) {
        $('#sub-page-num<?php echo $currentPage; ?>').css({"color": "red"});
      }
    });

    $("#submit_product_category").on("click", function() {
      $("#product_category_page").attr("value", "1");
    });

    $(".add-to-cart").click(function(e){
        e.preventDefault();
        let id = $(this).attr("value");
        let productId  = $("#productId"+id).val();
        let quantity = $("#quantity"+id).val();
        let prod = JSON.parse(sessionStorage.getItem('cart')); //array
        if (prod === null) {
          prod = JSON.parse("[]");
        }
        console.log(prod, Object.values(prod));
        let item = {
          "productId": productId,
          "quantity": quantity
        }
        prod.push(item);
        sessionStorage.setItem("cart", JSON.stringify(prod));

        //window.scrollTo(0, $("#top").offset().top);

        $("#success"+id).show(0).delay(5000).hide(0);
    });

    $("#shopping-cart").on("click", function (e) {
      e.preventDefault();
      //Create data object so we can submit it to the server 
      let data = {};
      for(let len = sessionStorage.length, i = 0; i < len; i++) {
          let key =  sessionStorage.key(i);
          data[key] = sessionStorage.getItem(key);
      }
      console.log(1);
      console.log(data);

      //From this point you can post the `data` to your server side
      /* Отправка через JQuery AJAX */
      /*$.ajax({
        url: '/cart',
        method: 'post',
        dataType: 'html',
        data: {'data': data},
        success: function(data){
          $('#result').html(data);
        }
      });*/

      function createCookie(name,value,days) {
        if (days) {
          var date = new Date();
          date.setTime(date.getTime()+(days*24*60*60*1000));
          var expires = "; expires="+date.toGMTString();
        }
        else var expires = "";
        document.cookie = name+"="+value+expires+"; path=/";
      }

      function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
          var c = ca[i];
          while (c.charAt(0)==' ') c = c.substring(1,c.length);
          if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
      }

      function eraseCookie(name) {
        createCookie(name,"",-1);
      }
      
    });

  });

  </script>



</main>
