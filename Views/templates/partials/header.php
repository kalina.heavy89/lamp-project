<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.82.0">
    <title>Album example · Bootstrap v5.0</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/album/">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">



    <!-- Bootstrap core CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/5.0/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/5.0/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/5.0/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/5.0/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/5.0/assets/img/favicons/safari-pinned-tab.svg" color="#7952b3">
<link rel="icon" href="/docs/5.0/assets/img/favicons/favicon.ico">
<meta name="theme-color" content="#7952b3">

<link rel="stylesheet" href="/css/style.css">

  <!-- Fontawesome -->
  <link href="../fontawesome/css/fontawesome.css" rel="stylesheet">
  <link href="../fontawesome/css/brands.css" rel="stylesheet">
  <link href="../fontawesome/css/solid.css" rel="stylesheet">

  </head>
  <body>

<header>
  <div class="navbar navbar-dark bg-dark shadow-sm">
    <div class="container">
      <a href="/" class="navbar-brand d-flex align-items-center" id="top">
        <i class="fas fa-shopping-basket"></i>
        <strong>&nbsp; MyShop</strong>
      </a>
      <div>
        <?php $categoryNames = ["All categories", "Food", "Clothes", "Shoes", "Dishes"];
        $categoryNumbers = ["*", "1", "2", "3", "4"];
        for ($i = 0; $i < count($categoryNames); $i++): ?>
          <span class="navbar-category">
            <a href="/?num_page=<?php echo '1' . "&product_category[]=" . $categoryNumbers[$i]; ?>"><?php echo $categoryNames[$i]; ?></a>
          </span>
        <?php endfor; ?>
      </div>
      <a href="/cart" class="btn btn-primary my-2" id="shopping-cart">Shopping Cart <span class="badge bg-info" id="badge-cart-size">Products: <?php echo $cartSize; ?></span></a>
      <a href="/" class="btn btn-primary my-2" id="resume-shopping">Resume Shopping</a>
      <a href="/" class="btn btn-primary my-2" id="products">Products</a>
    </div>
  </div>
</header>