<!DOCTYPE html>
<html lang="uk">
<head>
	<meta charset="utf-8">

	<style>
        h2, h3, table {
            text-align: center;
        }
        table {
            width: 60%;
            margin-left: auto;
            margin-right: auto;
        }
        li, .date, .footer {
            margin-left: 20%;
        }
        .cart-icon {
            width: 30px;
            height: 30px;
        }
        .fa {
            font-family: fontawesome;
            font-style: normal;
        }
    </style>
</head>
<body>
    <!--<h2><img class="cart-icon" src="/img/icons/shopping_cart.png"> My shop</h2>-->
    <!--<i class="fas fa-shopping-cart"></i>-->
    <br><br><br><br><br>
    <h2><i class="fa">&#xf07a;</i>&nbsp; My shop</h2>
    <h3>Your Order</h3>