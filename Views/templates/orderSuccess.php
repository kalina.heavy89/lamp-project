<main>
    <h2 class="card-title">Order</h2>
    <br>
    <div class="album py-5 bg-light">
        <div class="container order-table">
            <h2>Thank you for your order!</h2>
            <h2>We transferred the letter with order confirmation to your email.</h2>
        </div>
    </div>
</main>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
</script>
<script src="../scripts/cookies.js">
</script>
<script>

    $(document).ready(function(){
        $("#products").show();
        $('#resume-shopping').hide();
        $("#shopping-cart").hide();
        $(".navbar-category").hide();
        eraseCookie('cart');
    });

</script>