<?php

namespace LampProject\Views;

class View
{
    private $ext = 'php';
    private $folder = "templates";
    private $header = "header";
    private $headerPdf = "orderPdfHeader";
    private $footer = "footer";
    private $footerPdf = "orderPdfFooter";

    /**
     * Set extension for private variable $ext.
     *
     * @param string $ext
     * @return void
     */
    public function setExt(string $ext)
    {
        $this->ext = $ext;
    }

    /**
     * Set views folder for private variable $folder.
     *
     * @param string $dir
     * @return void
     */
    public function setViewsFolder(string $dir)
    {
        $this->folder = $dir;
    }

    /**
     * Assemble WEB-page from header, footer and template to render.
     *
     * @param string $viewName
     * @param array $data
     * @return void
     */
    public function render(string $viewName, array $data = [])
    {
        if (!empty($data)) {
            extract($data);
        }
        //header
        require ROOT_PATH . DIRECTORY_SEPARATOR . "Views" . DIRECTORY_SEPARATOR . $this->folder . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . $this->header . "." . $this->ext;

        require ROOT_PATH . DIRECTORY_SEPARATOR . "Views" . DIRECTORY_SEPARATOR . $this->folder . DIRECTORY_SEPARATOR . $viewName . "." . $this->ext;

        //footer
        require ROOT_PATH . DIRECTORY_SEPARATOR . "Views" . DIRECTORY_SEPARATOR . $this->folder . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . $this->footer . "." . $this->ext;
    }

    public function renderFull(string $viewName, array $data = [])
    {
        if (!empty($data)) {
            extract($data);
        }
        require ROOT_PATH . DIRECTORY_SEPARATOR . "Views" . DIRECTORY_SEPARATOR . $this->folder . DIRECTORY_SEPARATOR . $viewName . "." . $this->ext;
    }

    public static function filterString(string $dirtyString): string
    {
        //Replacing symbols \t\n\r\0\x0B to space
        $replSymbols = ["\t", "\r", "\n", "\0", "\x0B"];
        $dirtyString = str_replace($replSymbols, " ", $dirtyString);
        //Deleting repeated spaces
        return preg_replace('/^ +| +$|( ) +/m', '$1', $dirtyString);
    }

    public function createOrderPdf(array $orderData)
    {
        ob_start();
        //header
        require ROOT_PATH . DIRECTORY_SEPARATOR . "Views" . DIRECTORY_SEPARATOR . $this->folder . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . $this->headerPdf . "." . $this->ext;

        require ROOT_PATH . DIRECTORY_SEPARATOR . "Views" . DIRECTORY_SEPARATOR . $this->folder . DIRECTORY_SEPARATOR . "orderPdf" . "." . $this->ext;

        //footer
        require ROOT_PATH . DIRECTORY_SEPARATOR . "Views" . DIRECTORY_SEPARATOR . $this->folder . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . $this->footerPdf . "." . $this->ext;

        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }

}