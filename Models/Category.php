<?php

namespace LampProject\Models;

use LampProject\Models\Db;

class Category extends Db
{
    protected $id;
    public $name;
    protected $properties = ["id", "name"];

    public function __construct(
        int $id,
        string $name
    ) {
        $this->id = $id;
        $this->name = $name;
    }

    public static function getById($id): Category
    {
        $sql = "SELECT * FROM categories WHERE id=" . $id;
        $category = parent::getInstance()->query($sql)->fetch_assoc();
        return new Category(
            $id,
            $category['name']
        );
    }

}