<?php

namespace LampProject\Models;

class Db
{
    private static $_instance;

    private function __construct()
    {
        self::$_instance = new \mysqli(
            $_ENV['HOST'],
            $_ENV['DB_USER'],
            $_ENV['DB_PASSWORD'],
            $_ENV['DB_NAME']
        ) or die("Connect failed: %s\n". self::$_instance -> error);
    }

    public static function getInstance()
    {
        if (!self::$_instance) { // If no instance then make one
            new self();
            //echo("Connection successfully");
        }
        return self::$_instance;
        /*if (!function_exists('mysqli_init') && !extension_loaded('mysqli')) {
            echo 'We don\'t have mysqli!!!';
        } else {
            echo 'Phew we have it!';
        }*/
    }

    public function __call($name, $arguments)
    {
        // Замечание: значение $name регистрозависимо.
        if (strpos($name, "get") === 0) {
            $param = strtolower(explode("get", $name)[1]);
            if (in_array($param, $this->properties)) {
                return $this->{$param};
            }
        }

        if (strpos($name, "set") === 0) {
            $param = strtolower(explode("set", $name)[1]);
            if (in_array($param, $this->properties)) {
                $this->{$param} = $arguments[0];
            }
        }
    }

    function closeDBConnection()
    {
        self::$_instance -> close();
    }
}