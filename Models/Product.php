<?php

namespace LampProject\Models;

use LampProject\Models\Db;

class Product extends Db
{
    public $id;
    public $name;
    public $price;
    public $quantity;
    public $categoryId;
    public $image;
    public $properties = ["id", "name", "price", "quantity", "categoryId", "image"];

    public function __construct(
        int $id,
        string $name,
        float $price,
        int $quantity,
        int $categoryId,
        string $image = null
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->quantity = $quantity;
        $this->categoryId = $categoryId;
        if (!empty($image)) {
            $this->image = $image;
        }
    }

    public static function getAll(): array
    {
        $products = [];
        $stmt = Db::getInstance()->query("
            SELECT
                *
            FROM
                `products`
        ");
        $productsArr = $stmt->fetch_all();
        foreach ($productsArr as $product) {
            $products[] = new Product(
                $product[0],
                $product[1],
                $product[2],
                $product[3],
                $product[4],
                $product[5]
            );
        }
        return $products;
    }

    public static function getProductsWithPagination($limit, $page = 1, $category = ["*"]): array
    {
        if (!is_int($page)) {
            $page = (int) $page;
        }

        if (is_array($category)) {
            $stringCategory = implode (",", $category);
        } else {
            $stringCategory = (string) $category;
        }

        if ($stringCategory == "*") {
            $numRows = parent::getInstance()->query("SELECT count(*) FROM products")->fetch_all();
        } else {
            $numRows = parent::getInstance()->query("SELECT count(*) FROM products WHERE category_id IN (" . $stringCategory . ")")->fetch_all();
        }

        $strPages = ceil(((int) $numRows[0][0])/$limit);

        if ($page > $strPages) {
            $page = $strPages;
        }

        if ($page < 1) {
            $page = 1;
        }

        if ($stringCategory == "*" || $stringCategory == "0" || $stringCategory == null || $stringCategory == "") {
            $sql = "SELECT * FROM products LIMIT " . $limit . " OFFSET " . ($page - 1) * $limit;
        } else {
            $sql = "SELECT * FROM products WHERE category_id IN (" . $stringCategory . ") LIMIT " . $limit . " OFFSET " . ($page - 1) * $limit . "";
        }

        foreach (parent::getInstance()->query($sql)->fetch_all() as $product) {
            $products[] = new Product(
                $product[0],
                $product[1],
                $product[2],
                $product[3],
                $product[4],
                $product[5]
            );
        };

        $products['pages'] = $strPages;

        return $products;
    }

    public static function getById($id): Product
    {
        $sql = "SELECT * FROM products WHERE id=" . $id;

        $product = parent::getInstance()->query($sql)->fetch_assoc();

        return new Product(
            $id,
            $product['name'],
            $product['price'],
            $product['quantity'],
            $product['category_id'],
            $product['image']
        );
    }

}