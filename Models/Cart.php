<?php

namespace LampProject\Models;

use LampProject\Models\Db;

class Cart extends Db
{
    protected $id;
    protected $user_id;
    protected $total_price;
    protected $order_date;
    protected $properties = ["id", "user_id", "total_price", "order_date"];

    public function __construct(
        int $id,
        int $user_id,
        float $total_price,
        string $order_date
    ) {
        $this->id = $id;
        $this->user_id = $user_id;
        $this->total_price = $total_price;
        $this->order_date = $order_date;
    }
}